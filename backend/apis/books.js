const express = require('express')
var router = express.Router()
const mongodb = require('mongodb');

const DB_NAME = 'book_store'
const BOOK_COLLECTION_NAME = 'Books'

const DB_URI = 'mongodb+srv://michael:pass@cluster0-wlk2x.mongodb.net/test?retryWrites=true&w=majority';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

var objectID = require('mongodb').ObjectID

// Get request for BOOKS
router.get('/', function(req, res) {
  client.connect(function(err, connection) {
    const db = connection.db(DB_NAME); //connecting to the bookstore databse
    db.collection(BOOK_COLLECTION_NAME).find({}).toArray(function (find_err, records)  {
      if(find_err)
        return res.status(500).send(find_err)
      return res.send(records);
    });
  });
});

// Post Request for BOOKS
router.post('/', function(req, res) {
  if(!req.body || req.body.length === 0) //if it is null, it means they didnt send data
    return res.status(400).send({message: "record is required."});

  if(!req.body.title || !req.body.author || !req.body.price)
  return res.status(400).send({message: "fix ur data. Title, author, and price are required."});

  client.connect(function(err, connection) {
    const db = connection.db(DB_NAME); //connecting to the bookstore databse

    db.collection(BOOK_COLLECTION_NAME).insertOne(req.body, function(insert_error, data){
      if(insert_error)
        return res.status(500).send({message: "something went wrong."}); //HTTP status code page

      connection.close()
      return res.status(200).send({message: "Your book has been inserted successfully"}) //if it actually works (thats what 200 is)
    });
  })
})

// Put request for BOOKS
//localhost:5050/123455
//if I add more after /:id, like /:id/:name, itll parse data into id and name
router.put('/:id', function(req, res) {
    client.connect(function(err, connection) {
      if(err)
        return res.status(500).send({error: err}) //because it was a server error, we use 500

      if(!req.body || req.body.length === 0) //if it is null, it means they didnt send data
        return res.status(400).send({message: "record is required."});

      const db = connection.db(DB_NAME)
      db.collection(BOOK_COLLECTION_NAME)
        .updateOne({_id: objectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
            if(update_err)
              return res.status(500).send({error: "Couldn't update book records"}) //because it was a server error, we use 500
            return res.status(200).send({error: "update was successful", data: update_data})
        })
    })
})

router.delete('/:id', function(req, res){
  client.connect(function(err, connection) {
    if(err)
      return res.status(500).send({error: err}) //because it was a server error, we use 500

      const db = connection.db(DB_NAME)
      db.collection(BOOK_COLLECTION_NAME).deleteOne({"_id": objectID( req.params.id)}, function(err, data){
        if(err)
          return res.status(500).send({error: "The removal was unsuccessful.", data: data})
        return res.status(200).send({error: "this actually worked", data: data})
      })
    })
  })

module.exports = router;
