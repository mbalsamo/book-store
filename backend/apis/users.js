const express = require('express')
var router = express.Router()
const mongodb = require('mongodb');

const DB_NAME = 'book_store'
const USERS_COLLECTION_NAME = 'Users'

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });


router.get('/', function(req, res) {
  client.connect(function(err, connection) {
    const db = connection.db(DB_NAME); //connecting to the bookstore databse
    db.collection(USERS_COLLECTION_NAME).find({}).toArray(function (find_err, records)  {
      if(find_err)
        return res.status(500).send(find_err)
      res.send(records);
    });
  })
})

router.post('/', function(req, res) {
  if(!req.body || req.body.length === 0) //if it is null, it means they didnt send data
    return res.status(400).send({message: "record is required."});

  if(!req.body.name || !req.body.email || !req.body.card)
    return res.status(400).send({message: "You need to at least enter your name, email, and card information!"});

  client.connect(function(err, connection) {
    const db = connection.db(DB_NAME); //connecting to the bookstore databse

    db.collection(USERS_COLLECTION_NAME).insertOne(req.body, function(insert_error, data){
      if(insert_error)
        return res.status(500).send({message: "something went wrong."}); //HTTP status code page

      connection.close()
      return res.status(200).send({message: "The user has been inserted successfully", data:req.body}) //if it actually works (thats what 200 is)
  });
})
})

router.put('/:id', function(req, res) {
  client.connect(function(err, connection) {
    if(err)
      return res.status(500).send({error: err})

    if(!req.body || req.body.length === 0) //if it is null, it means they didnt send data
      return res.status(400).send({message: "record is required."});

    const db = connection.db(DB_NAME)
    db.collection(USERS_COLLECTION_NAME)
      .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
          if(update_err)
            return res.status(500).send({error: "Couldn't update user records"}) //because it was a server error, we use 500
          return res.status(200).send({error: "update was successful", data: update_data})
      })
  })
})

router.delete('/:id', function(req, res){
  client.connect(function(err, connection) {
    if(err)
      return res.status(500).send({error: err}) //because it was a server error, we use 500

      const db = connection.db(DB_NAME)
      db.collection(USERS_COLLECTION_NAME).deleteOne(req.body, function(err, data){
        if(err)
          return res.status(500).send({error: "The removal was unsuccessful.", data: date})

        return res.status(200).send({error: "this actually worked", data: data})
      })
  })
})

module.exports = router;
