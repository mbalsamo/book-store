const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const books = require('./apis/books'); //go and get the books.js file
const users = require('./apis/users') //go get users.js
const orders = require('./apis/orders') //get orders.js

const app = express();
const PORT = 5050;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Content-Type")
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE")

  next();
})


app.use(bodyParser.urlencoded({extended: false})); //allow user to send data within URL
app.use(bodyParser.json()); //allow user to send JSON data

//all the books endpoints
app.use('/books', books);
//all the users endpoints
app.use('/users', users)
//all the orders endpoints
app.use('/orders', orders)

app.listen(PORT);
console.log("Listening on port " + PORT);
