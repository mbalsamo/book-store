import { Component, OnInit } from '@angular/core';
import { BooksService } from './books.service'

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  name: string= "MICHAEL BALASAAMAMO"
  books: any[]

  constructor(private booksService: BooksService) { }


  ngOnInit() {
    // subscribe means that whenever the data is available
    // (when it gets books) then it does whats in subscribe
    this.booksService.getAllBooks().subscribe(
      data => { // mapping, or a function in javascript
        this.books = data
        console.log(data)
      },
      error => {
        console.log(error)
      }
    )
  }

}
